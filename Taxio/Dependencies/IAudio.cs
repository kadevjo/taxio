﻿using System;

namespace Taxio.Dependencies
{
	public interface IAudio
	{
		bool PlayMp3File(string fileName);
	}
}