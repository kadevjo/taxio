﻿using System.Threading.Tasks;
using System.Collections.Generic;

namespace Taxio.Dependencies
{
    public interface IGeneralServices
    {
		void ClearNotifications ();
        Task<Dictionary<string, double>> GetUserLocation();
		string GetDeviceId ();
		Task IdentifyInstalltion(string push_id); // Implemet for iOS
    }
}
