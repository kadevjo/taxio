﻿using System;

namespace Taxio.Dependencies
{
	public interface ICacheable
	{
		string CacheId { get; set; }
	}
}