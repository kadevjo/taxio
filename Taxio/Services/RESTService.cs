﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using RestSharp.Portable;
using Newtonsoft.Json;
using Taxio.Helpers;

namespace Kadevjo
{
	public class ParseResponse<T>
	{
		public List<T> Results { get; set; }
	}

	public abstract class RESTService<I,T> where I : new()
	{
		private static I instance;

		public static I Instance {
			get { 
				if (instance == null) {
					instance = new I ();
				}

				return instance;
			}
		}

		public static string BaseUrl {
			get { 
				return "https://taxioapp.com/api/v1/fast_ride/driver/";
			}
		}

		public static Dictionary<string,string> Headers {
			get { 
				return new Dictionary<string,string> () {
					{ "Authorization", "Token token=c86a667d4e794541baf5745f880568c9" },
				};
			}
		}

		private static RestClient client;

		protected static RestClient Client {
			get { 
				if (client == null) {
					if (string.IsNullOrEmpty (BaseUrl)) {
						throw new ArgumentNullException ("BaseUrl");
					}

					client = new RestClient (BaseUrl);
					if (Headers != null) {
						foreach (var kvp in Headers) {
							client.AddDefaultParameter (kvp.Key, kvp.Value, ParameterType.HttpHeader);
						}
					}
				}

				return client;
			}
		}

		protected abstract string Resource { get; }

		public async virtual Task<T> RequestHandler (HttpMethod method, Dictionary<string,object> parameters, string resource = "")
		{
			var request = CreateRequest (Resource + resource, method, parameters);
			try {
				var response = await Client.Execute<T> (request);
				return response.Data;
			} catch (Exception ex) {
				System.Diagnostics.Debug.WriteLine (ex.Message);
				return default(T);
			}
		}

		protected RestRequest CreateRequest (string resource, HttpMethod method,
		                                     Dictionary<string,object> parameters = null)
		{
			string deviceId;
			if (Settings.DeviceId == string.Empty) {
				deviceId = Generals.GetDeviceId ();
				Settings.DeviceId = deviceId;
			} else {
				deviceId = Settings.DeviceId;
			}
			RestRequest request = new RestRequest (resource, method);

			if (method == HttpMethod.Put || method == HttpMethod.Delete) {
				request.AddParameter ("device_id", deviceId, ParameterType.QueryString);
			} else {
				request.AddParameter ("device_id", deviceId, ParameterType.GetOrPost);
			}

			if (parameters != null) {
				foreach (KeyValuePair<string,object> kvp in parameters) {
					if (method == HttpMethod.Put || method == HttpMethod.Delete) {
						request.AddParameter (kvp.Key, kvp.Value, ParameterType.QueryString);
					} else {
						request.AddParameter (kvp.Key, kvp.Value, ParameterType.GetOrPost);
					}
				}
			}
			return request;
		}
	}
}