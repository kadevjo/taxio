﻿using Kadevjo;
using Taxio.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using System;
using Taxio.Helpers;

namespace Taxio.Services
{
	public class CabsService : RESTService <CabsService, Response>
	{
		protected override string Resource {
			get {
				return "cabs";
			}
		}

		public async Task<Response> Login (User user)
		{
			var resources = "/login";
			var parameters = new Dictionary<string,object> () {
				{ "company",user.Company },
				{ "cab",user.Cab }
			};
			var response = await RequestHandler (HttpMethod.Post, parameters, resources);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Can't login");
			#endif
			return response;
		}

		public async Task<Response> Logout (User user)
		{			
			var resources = "/logout";
			var parameters = new Dictionary<string,object> () {
				{ "company",user.Company },
				{ "cab",user.Cab }
			};
			var response = await RequestHandler (HttpMethod.Delete, parameters, resources);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Can't logout");
			#endif
			return response;
		}

		public async Task<Response> SendCurrentLocation (Location currentLocation)
		{
			if (string.IsNullOrEmpty (Settings.Cab) || string.IsNullOrEmpty (Settings.Company)) {
				#if DEBUG
				System.Diagnostics.Debug.WriteLine ("Cab or Company parameter is empty - Send Current Location");
				#endif
				return default(Response);
			}

			var resources = "/geocode";
			var location = new Dictionary<string,object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
				{ "latitude" , currentLocation.latitude },
				{ "longitude" , currentLocation.longitude },
			};

			var response = await RequestHandler (HttpMethod.Put, location, resources);

			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Current location won't send");
			#endif

			return response;
		}

		public async Task<Response> SearchTrip ()
		{
			if (string.IsNullOrEmpty (Settings.Cab) || string.IsNullOrEmpty (Settings.Company)) {
				#if DEBUG
				System.Diagnostics.Debug.WriteLine ("Cab or Company parameter is empty - Search Trip");
				#endif
				return default(Response);
			}

			var resources = "/next_trip";
			var parameters = new Dictionary<string, object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
			};
			var response = await RequestHandler (HttpMethod.Get, parameters, resources);

			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("No Trip Found");
			#endif

			return response;
		}
	}
}

