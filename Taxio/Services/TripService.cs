﻿using Kadevjo;
using Taxio.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Generic;
using Taxio.Helpers;

namespace Taxio.Services
{
	public class TripService : RESTService <TripService, Response>
	{
		protected override string Resource {
			get {
				return "trips";
			}
		}

		public async Task<Response> DeclineTrip (string tripId)
		{
			var resource = "/exec_code_1010";

			var parameters = new Dictionary<string,object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
				{ "trip_id",tripId }
			};
			var response = await RequestHandler (HttpMethod.Put, parameters, resource);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Couldn't send decline");
			#endif
			return response;
		}

		public async Task<Response> AcceptTrip (string tripId)
		{
			var resource = "/exec_code_104";

			var parameters = new Dictionary<string,object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
				{ "trip_id",tripId }
			};
			var response = await RequestHandler (HttpMethod.Put, parameters, resource);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Couldn't send acceptation");
			#endif
			return response;
		}

		public async Task<Response> DriverArrived (string tripId)
		{
			var resource = "/exec_code_20";

			var parameters = new Dictionary<string,object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
				{ "trip_id",tripId }
			};
			var response = await RequestHandler (HttpMethod.Put, parameters, resource);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Couldn't execute Driver Arrived");
			#endif
			return response;
		}

		public async Task<Response> CallOnce (string tripId)
		{
			var resource = "/exec_code_21";

			var parameters = new Dictionary<string,object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
				{ "trip_id",tripId }
			};
			var response = await RequestHandler (HttpMethod.Put, parameters, resource);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Couldn't execute Call Once");
			#endif
			return response;
		}

		public async Task<Response> ContactPassenger (string tripId)
		{
			var resource = "/exec_code_22";

			var parameters = new Dictionary<string,object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
				{ "trip_id",tripId }
			};
			var response = await RequestHandler (HttpMethod.Put, parameters, resource);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Couldn't Contact Passenger");
			#endif
			return response;
		}

		public async Task<Response> ClientPicked (string tripId)
		{
			var resource = "/exec_code_1011";

			var parameters = new Dictionary<string,object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
				{ "trip_id",tripId }
			};
			var response = await RequestHandler (HttpMethod.Put, parameters, resource);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Couldn't execute Client Picked");
			#endif
			return response;
		}

		public async Task<Response> ClientDropped (string tripId)
		{
			var resource = "/exec_code_99";

			var parameters = new Dictionary<string,object> () {
				{ "company", Settings.Company },
				{ "cab", Settings.Cab },
				{ "trip_id",tripId }
			};
			var response = await RequestHandler (HttpMethod.Put, parameters, resource);
			#if DEBUG
			if (response == default(Response))
				System.Diagnostics.Debug.WriteLine ("Couldn't execute Client Dropped");
			#endif
			return response;
		}
	}
}

