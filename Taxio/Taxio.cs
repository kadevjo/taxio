﻿using System;

using Xamarin.Forms;
using Taxio.Pages;
using Taxio.Helpers;
using Taxio.Models;
using System.Collections.Generic;
using Taxio.Dependencies;

namespace Taxio
{
	public class App : Application
	{
		public static Response Args;
		public static SendCurrentLocation SendCurrentLocationTask;
		public static SearchTrip SearchTripTask;

		public App ()
		{
			SendCurrentLocationTask = new SendCurrentLocation ();
			SendCurrentLocationTask.Initializer ();

			SearchTripTask = new SearchTrip ();

			if (Settings.Company != string.Empty && Settings.Cab != string.Empty) {
				if (Args != null) {
					MainPage = new ConfirmationPage (Args.trip);
				} else {
					MainPage = Generals.CreateNavPage (new TripPage (null));
				}
			} else {
				MainPage = new LoginPage ();
			}
		}

		protected override void OnStart ()
		{
			DependencyService.Get<IGeneralServices> ().ClearNotifications ();
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			DependencyService.Get<IGeneralServices> ().ClearNotifications ();
		}
	}
}