﻿using System;
using Taxio.Dependencies;

namespace Taxio.Models
{
	public class Trip: ICacheable
	{
		public string CacheId { get { return id; } set { } }
		public string pickup{ get; set; }
		public DateTime pickup_at{ get; set; }
		public string id{ get; set; }
		public string notes{ get; set; }
		public Location geolocation{ get; set; }
	}
}

