﻿using System;

namespace Taxio.Models
{
	public class Response
	{
		public bool success{ get; set; }

		public Error error { get; set; }

		public bool trip_assigned{ get; set; }

		public Trip trip{ get; set; }

		public string push_id { get; set; }
	}

	public class Error
	{
		public string code{ get; set; }

		public string message{ get; set; }
	}
}

