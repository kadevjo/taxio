// Helpers/Settings.cs
using Refractored.Xam.Settings;
using Refractored.Xam.Settings.Abstractions;

namespace Taxio.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings {
			get {
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;

		private const string ActiveKey = "Active";
		private static readonly string ActiveDefault = "False";

		private const string CompanyKey = "Company";
		private static readonly string CompanyDefault = string.Empty;

		private const string CabKey = "Cab";
		private static readonly string CabDefault = string.Empty;

		private const string DeviceIdKey = "DeviceId";
		private static readonly string DeviceIdDefault = string.Empty;

		private const string ParseAppIdKey = "ParseAppId";
		private static readonly string ParseAppIdDefault = "MrOlyWkifywaq9tt5vDO6BOta1fENy53WaBJ5X0R"; //Testing FastRideDriver (Testing) Parse App

		private const string ParseAppKeyKey = "ParseAppKey";
		private static readonly string ParseAppKeyDefault = "NWy5gQsIs0dTSd0psRMY64LyQTmzJsNtvdbDHSsQ"; //Testing FastRideDriver (Testing) Parse App .NET Key

		#endregion


		public static string GeneralSettings {
			get {
				return AppSettings.GetValueOrDefault (SettingsKey, SettingsDefault);
			}
			set {
				AppSettings.AddOrUpdateValue (SettingsKey, value);
			}
		}

		public static string Active {
			get {
				return AppSettings.GetValueOrDefault (ActiveKey, ActiveDefault);
			}
			set {
				AppSettings.AddOrUpdateValue (ActiveKey, value);
			}
		}

		public static string Company {
			get {
				return AppSettings.GetValueOrDefault (CompanyKey, CompanyDefault);
			}
			set {
				AppSettings.AddOrUpdateValue (CompanyKey, value);
			}
		}

		public static string Cab {
			get {
				return AppSettings.GetValueOrDefault (CabKey, CabDefault);
			}
			set {
				AppSettings.AddOrUpdateValue (CabKey, value);
			}
		}

		public static string DeviceId {
			get {
				return AppSettings.GetValueOrDefault (DeviceIdKey, DeviceIdDefault);
			}
			set {
				AppSettings.AddOrUpdateValue (DeviceIdKey, value);
			}
		}

		public static string ParseAppId {
			get {
				return AppSettings.GetValueOrDefault (ParseAppIdKey, ParseAppIdDefault);
			}
			set {
				AppSettings.AddOrUpdateValue (ParseAppIdKey, value);
			}
		}

		public static string ParseAppKey {
			get {
				return AppSettings.GetValueOrDefault (ParseAppKeyKey, ParseAppKeyDefault);
			}
			set {
				AppSettings.AddOrUpdateValue (ParseAppKeyKey, value);
			}
		}
	}
}