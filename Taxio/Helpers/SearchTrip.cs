﻿using System;
using System.Threading.Tasks;
using Taxio.Helpers;
using Taxio.Services;
using Taxio.Models;
using System.Threading;
using Xamarin.Forms;
using System.Collections.Generic;
using Taxio.Pages;

namespace Taxio.Helpers
{
	public class SearchTrip
	{
		public int ServerConnetionErrorsCounter;

		private CancellationTokenSource tokenSource;
		private CancellationToken token;

		public void Initializer ()
		{
			ServerConnetionErrorsCounter = 0;

			tokenSource = new CancellationTokenSource ();
			token = tokenSource.Token;

			Send ().ConfigureAwait (false);

			Timer (token);
		}

		private void Timer (CancellationToken token)
		{
			if (token.IsCancellationRequested == false) {
				Device.StartTimer (new TimeSpan (0, 0, Generals.TimerLap), () => {
					Search (token);
					return false;
				});
			}
		}

		private async Task Search (CancellationToken token)
		{
			if (token.IsCancellationRequested == false) {
				await Send ();
				System.Diagnostics.Debug.WriteLine ("SearchTripTask method hitted");
				Timer (token);
			}
		}

		public async Task Send ()
		{
			var response = await CabsService.Instance.SearchTrip ();
			if (response == null) {
				ServerConnetionErrorsCounter++;
				if (ServerConnetionErrorsCounter >= 4) {
					var arg = "ServerConnectionWarning";
					MessagingCenter.Send<SearchTrip,Dictionary<string, object>> (this, Generals.SearchTripResult,
						new Dictionary<string, object> {
							{ "message",arg },
							{ "response",response }
						});
				}
			} else {
				NewTripHandler (response);
			}
		}

		public void NewTripHandler (Response response)
		{
			if (response != default(Response) && response.trip != null) {
				if (response.success && response.trip_assigned && Settings.Active == "False") {
					App.Args = default(Response);
					App.Current.MainPage = new ConfirmationPage (response.trip);
				}
			}
		}

		public void Cancel ()
		{
			tokenSource.Cancel ();
		}
	}
}

