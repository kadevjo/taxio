﻿using System;
using System.Threading.Tasks;
using Taxio.Helpers;
using Taxio.Services;
using Taxio.Models;
using System.Threading;
using Xamarin.Forms;

namespace Taxio.Helpers
{
	public class SendCurrentLocation
	{
		public int LocationErrorsCounter;
		public int ServerConnetionErrorsCounter;

		private CancellationTokenSource tokenSource;
		private CancellationToken token;

		public void Initializer ()
		{
			LocationErrorsCounter = 0;
			ServerConnetionErrorsCounter = 0;

			tokenSource = new CancellationTokenSource ();
			token = tokenSource.Token;

			Send ().ConfigureAwait(false);

			Timer (token);
		}

		private void Timer (CancellationToken token)
		{
			if (token.IsCancellationRequested == false) {
				Device.StartTimer (new TimeSpan (0, 0, Generals.TimerLap), () => {
					SendLocationTask (token);
					return false;
				});
			}
		}

		private async Task SendLocationTask (CancellationToken token)
		{
			if (token.IsCancellationRequested == false) {
				await Send ();
				System.Diagnostics.Debug.WriteLine ("SendLocationTask method hitted");
				Timer (token);
			}
		}

		public async Task Send ()
		{
			var arg = string.Empty;
			var generals = new Generals ();
			var location = await generals.CurrentLocation ();
			if (location [0] == 0 && location [1] == 0) {
				LocationErrorsCounter++;
				if (LocationErrorsCounter >= 4) {
					arg = "LocationWarning";
				}
			} else {
				LocationErrorsCounter = 0;
				var currentLocation = new Location {
					latitude = location [0],
					longitude = location [1],
				};
				var response = await CabsService.Instance.SendCurrentLocation (currentLocation);
				if (response == default(Response)) {
					ServerConnetionErrorsCounter++;
					if (ServerConnetionErrorsCounter >= 4) {
						arg = "ServerConnectionWarning";
					}
				} else {
					arg = "Send";
					ServerConnetionErrorsCounter = 0;
				}
			}
			MessagingCenter.Send<SendCurrentLocation,string> (this, Generals.SendCurrentLocationResult, arg);
		}

		public void Cancel ()
		{
			tokenSource.Cancel ();
		}
	}
}

