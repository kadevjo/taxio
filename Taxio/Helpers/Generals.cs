﻿using Xamarin.Forms;
using Taxio.Dependencies;
using System.Linq;
using System.Threading.Tasks;
using System;
using Taxio.Pages;
using Taxio.Services;
using Taxio.Models;

namespace Taxio.Helpers
{
	public class Generals
	{
		// Lap sould be 15 on production
		#if DEBUG
		public static int TimerLap { get { return 5; } }
		#else
		public static int TimerLap { get { return 15; } }
		#endif

		public static string SendCurrentLocationResult { get { return "SendCurrentLocationResult"; } }
		public static string SearchTripResult { get { return "SearchTripResult"; } }

		public class Palette
		{
			public static Color TaxioPrimary { get { return Color.FromHex ("#24C2CD"); } } // Blue
			public static Color TaxioAlert { get { return Color.FromHex ("#E54523"); } } // Red
			public static Color TaxioWarning { get { return Color.FromHex ("#FC9171"); } } // Orange
			public static Color TaxioCodeButton { get { return Color.FromHex ("#B4F2E3"); } } //LightAqua
			public static Color TaxioDefault { get { return Color.FromHex ("#EAEAEA"); } } // LightGray
			public static Color TaxioTitle { get { return Color.FromHex ("#353535"); } } // LightGray

			public static Color TaxioCodeButtonOff { get { return Color.FromHex ("#95F8FE"); } } // GreenOff
			public static Color TaxioStandByBackground { get { return Color.FromHex ("#FFFFFF"); } } // Yellow
		};

		public async Task<double []> CurrentLocation ()
		{
			try {
				var geolocation = DependencyService.Get<IGeneralServices> ();
				var location = await geolocation.GetUserLocation ();
				var latitude = location.FirstOrDefault (k => k.Key.Contains ("Latitude")).Value;
				var longitude = location.FirstOrDefault (k => k.Key.Contains ("Longitude")).Value;
				return new double []{ latitude, longitude };
			} catch (Exception ex) {
				return new double []{ 0, 0 };
			}
		}

		public static string GetDeviceId ()
		{
			try {
				var generals = DependencyService.Get<IGeneralServices> ();
				return generals.GetDeviceId ();
			} catch (Exception ex) {
				return string.Empty;
			}
		}

		public static NavigationPage CreateNavPage (ContentPage NewPage)
		{
			var mainNav = new NavigationPage (NewPage);
			mainNav.BarBackgroundColor = Generals.Palette.TaxioPrimary;
			mainNav.BarTextColor = Color.White;
			return mainNav;
		}

		public async static Task<bool> Login (User user)
		{
			var response = await CabsService.Instance.Login (user);
			if (response != default(Response) && response.success) {
				Settings.Company = user.Company;
				Settings.Cab = user.Cab;

				var general = DependencyService.Get<IGeneralServices> ();
				await general.IdentifyInstalltion (response.push_id);
				return true;
			}
			return false;
		}

		public async static Task<bool> Logout ()
		{
			var user = new User {
				Company = Settings.Company,
				Cab = Settings.Cab,
			};
			var response = await CabsService.Instance.Logout (user);
			if (response != default(Response) && response.success) {
				Settings.Company = string.Empty;
				Settings.Cab = string.Empty;
				// Deleting cache
				try {
					var cache = DependencyService.Get<ICache> ();
					await cache.RemoveObjects<Trip> ();
				} catch (Exception ex) {
					System.Diagnostics.Debug.WriteLine (ex.Message);
				}
				App.SendCurrentLocationTask.Cancel ();
				return true;
			}
			return false;
		}


		public async static Task AnimateButton (Button button)
		{
			await button.ScaleTo (0.9, 150, Easing.CubicInOut);
			await button.ScaleTo (1, 150, Easing.CubicInOut);
		}

		public async static Task OKFeedbackButtonAnimation (Button button)
		{
			var currentBackgroudColor = button.BackgroundColor;
			var currentTextColor = button.TextColor;

			button.TextColor = Color.White;
			button.BackgroundColor = Generals.Palette.TaxioPrimary;

			await button.FadeTo (0.5, 0, Easing.CubicInOut);
			await button.FadeTo (1, 250, Easing.CubicInOut);
			await button.FadeTo (0.5, 0, Easing.CubicInOut);
			await button.FadeTo (1, 250, Easing.CubicInOut);

			button.BackgroundColor = currentBackgroudColor;
			button.TextColor = currentTextColor;
		}
	}
}