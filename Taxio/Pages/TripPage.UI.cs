﻿using Xamarin.Forms;
using Taxio.Helpers;

namespace Taxio.Pages
{
	public partial class TripPage
	{
		private Label DetailsLabel;
		private Label AddressLabel;
		private Button ContactPassengerButton;
		public Button Code20Button;
		public Button Code21Button;
		public Button Code1011Button;
		public Button Code99Button;
		private StackLayout AlertLayout;
		private Label TitleAlertLabel;
		private Label MessageAlertLabel;
		private ActivityIndicator activityIndicator;

		private StackLayout SuccessLayout;
		private Label TitleSuccessLabel;
		private Label MessageSuccessLabel;

		public void InitializeComponents ()
		{
			Title = "Fast Ride";

			var MainLayout = new RelativeLayout () {
				BackgroundColor = Color.White,
			};

			var TitleLabel = new Label {
				Text = AppResources.ActiveTrip,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = Generals.Palette.TaxioTitle,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Center,
			};
			MainLayout.Children.Add (TitleLabel,
				Constraint.Constant (20),
				Constraint.Constant (30),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			var InfoBackground = new BoxView {
				BackgroundColor = Generals.Palette.TaxioDefault,
			};
			MainLayout.Children.Add (InfoBackground,
				Constraint.Constant (20),
				Constraint.RelativeToView (TitleLabel, (p, v) => v.Y + v.Height + 30),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (135)
			);

			var AddressTitleLabel = new Label {
				Text = AppResources.Address,
				FontAttributes = FontAttributes.Bold,
				TextColor = Generals.Palette.TaxioWarning,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			var AddressTitleLabelTapRecognizer = new TapGestureRecognizer ();
			AddressTitleLabelTapRecognizer.Tapped += AddressTitleLabelTap_Tapped;
			AddressTitleLabel.GestureRecognizers.Add (AddressTitleLabelTapRecognizer);
			MainLayout.Children.Add (AddressTitleLabel,
				Constraint.Constant (25),
				Constraint.RelativeToView (TitleLabel, (p, v) => v.Y + v.Height + 30),
				Constraint.RelativeToParent ((p) => p.Width - 50),
				Constraint.Constant (30)
			);

			AddressLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Generals.Palette.TaxioTitle,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			var AddressLabelTapRecognizer = new TapGestureRecognizer ();
			AddressLabelTapRecognizer.Tapped += AddressTitleLabelTap_Tapped;
			AddressLabel.GestureRecognizers.Add (AddressLabelTapRecognizer);
			MainLayout.Children.Add (AddressLabel,
				Constraint.Constant (25),
				Constraint.RelativeToView (AddressTitleLabel, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 50),
				Constraint.Constant (30)
			);

			var DetailsTitleLabel = new Label {
				Text = AppResources.Details,
				FontAttributes = FontAttributes.Bold,
				TextColor = Generals.Palette.TaxioWarning,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (DetailsTitleLabel,
				Constraint.Constant (25),
				Constraint.RelativeToView (AddressLabel, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 50),
				Constraint.Constant (30)
			);

			DetailsLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Generals.Palette.TaxioTitle,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (DetailsLabel,
				Constraint.Constant (25),
				Constraint.RelativeToView (DetailsTitleLabel, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 50),
				Constraint.Constant (30)
			);

			Code20Button = new Button {
				Text = AppResources.Code20,
				TextColor = Generals.Palette.TaxioPrimary,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Generals.Palette.TaxioCodeButtonOff,
				//BorderColor = Generals.Palette.TaxioBlue,
				//BorderWidth = 1,
				BorderRadius = 0,
			};
			Code20Button.Clicked += Code20Button_Clicked;
			MainLayout.Children.Add (Code20Button,
				Constraint.Constant (20),
				Constraint.RelativeToView (DetailsLabel, (p, v) => v.Y + v.Height + 50),
				Constraint.RelativeToParent ((p) => p.Width - 30 - (p.Width / 2)),
				Constraint.Constant (40)
			);

			Code21Button = new Button {
				Text = AppResources.Code21,
				TextColor = Generals.Palette.TaxioPrimary,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Generals.Palette.TaxioCodeButtonOff,
				//BorderColor = Generals.Palette.TaxioBlue,
				//BorderWidth = 1,
				BorderRadius = 0,
			};
			Code21Button.Clicked += Code21Button_Clicked;
			MainLayout.Children.Add (Code21Button,
				Constraint.RelativeToParent ((p) => p.Width - p.Width / 2 + 10),
				Constraint.RelativeToView (DetailsLabel, (p, v) => v.Y + v.Height + 50),
				Constraint.RelativeToParent ((p) => p.Width - 30 - (p.Width / 2)),
				Constraint.Constant (40)
			);

			Code1011Button = new Button {
				Text = AppResources.Code1011,
				TextColor = Generals.Palette.TaxioPrimary,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Generals.Palette.TaxioCodeButtonOff,
				//BorderColor = Generals.Palette.TaxioBlue,
				//BorderWidth = 1,
				BorderRadius = 0,
			};
			Code1011Button.Clicked += Code1011Button_Clicked;
			MainLayout.Children.Add (Code1011Button,
				Constraint.Constant (20),
				Constraint.RelativeToView (Code20Button, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 30 - (p.Width / 2)),
				Constraint.Constant (40)
			);

			Code99Button = new Button {
				Text = AppResources.Code99,
				TextColor = Generals.Palette.TaxioPrimary,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Generals.Palette.TaxioCodeButtonOff,
				//BorderColor = Generals.Palette.TaxioBlue,
				//BorderWidth = 1,
				BorderRadius = 0,
			};
			Code99Button.Clicked += Code99Button_Clicked;
			MainLayout.Children.Add (Code99Button,
				Constraint.RelativeToParent ((p) => p.Width - p.Width / 2 + 10),
				Constraint.RelativeToView (Code20Button, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 30 - (p.Width / 2)),
				Constraint.Constant (40)
			);

			//Warnings alerts
			TitleAlertLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = Color.White,
			};
			MessageAlertLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Color.White,
			};
			AlertLayout = new StackLayout {
				BackgroundColor = Generals.Palette.TaxioAlert,
				Padding = new Thickness (10, 10, 10, 10),
				Children = { TitleAlertLabel, MessageAlertLabel },
				IsVisible = false,
			};
			MainLayout.Children.Add (AlertLayout,
				Constraint.Constant (20),
				Constraint.RelativeToView (Code99Button, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (70)
			);

			//Success alerts
			TitleSuccessLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = Color.White,
			};
			MessageSuccessLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Color.White,
			};
			SuccessLayout = new StackLayout {
				BackgroundColor = Generals.Palette.TaxioPrimary,
				Padding = new Thickness (10, 10, 10, 10),
				Children = { TitleSuccessLabel, MessageSuccessLabel },
				IsVisible = false,
			};
			MainLayout.Children.Add (SuccessLayout,
				Constraint.Constant (20),
				Constraint.RelativeToView (Code99Button, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (70)
			);

			activityIndicator = new ActivityIndicator {
				IsVisible = false,
				IsRunning = true,
				Color = Generals.Palette.TaxioPrimary,
			};
			MainLayout.Children.Add (activityIndicator,
				Constraint.Constant (20),
				Constraint.RelativeToView (Code99Button, (p, v) => v.Y + v.Height + 5),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (50)
			);

			ContactPassengerButton = new Button {
				Text = AppResources.ContactPassenger,
				TextColor = Generals.Palette.TaxioPrimary,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Generals.Palette.TaxioCodeButtonOff,
				//BorderColor = Generals.Palette.TaxioBlue,
				//BorderWidth = 1,
				BorderRadius = 0,
			};
			ContactPassengerButton.Clicked += ContactPassengerButton_Clicked;
			MainLayout.Children.Add (ContactPassengerButton,
				Constraint.Constant (20),
				Constraint.RelativeToView (AlertLayout, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (40)
			);

			var LogoutToolbarButton = new ToolbarItem {
				Icon = "ic_logout.png",
			};
			LogoutToolbarButton.Clicked += LogoutButton_Clicked;
			ToolbarItems.Add (LogoutToolbarButton);

			Content = new ScrollView { Content = MainLayout, Padding = new Thickness (0, 0, 0, 10) };

		}
	}
}


