﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using Taxio.Dependencies;
using System.Linq;
using Taxio.Helpers;
using System;
using Taxio.Services;
using Taxio.Models;
using System.Collections.Generic;
using Refractored.Xam.Vibrate.Abstractions;

namespace Taxio.Pages
{
	public partial class MapsPage : ContentPage
	{
		public bool TimerStopper;
		public int LocationErrorsCounter;
		public int ServerConnetionErrorsCounter;
		public Trip CurrentTrip;

		public MapsPage (Trip trip)
		{
			CurrentTrip = trip;
			InitializeComponents ();
			LoadData ().ConfigureAwait (false);
		}

		public async Task LoadData ()
		{
			AddressLabel.Text = CurrentTrip.pickup == null ? string.Empty : CurrentTrip.pickup;
			DetailsLabel.Text = CurrentTrip.pickup_at == null ? string.Empty
				: CurrentTrip.pickup_at.ToString ("HH:mm dd/MM")
				+ " - " + (string.IsNullOrEmpty (CurrentTrip.notes) ? AppResources.NoDetailsTrip : CurrentTrip.notes);

			map.MoveToRegion (
				MapSpan.FromCenterAndRadius (
					new Position (CurrentTrip.geolocation.latitude, CurrentTrip.geolocation.longitude),
					Distance.FromMiles (0.3)
				)
			);
			var position = new Position (CurrentTrip.geolocation.latitude, CurrentTrip.geolocation.longitude);
			var pin = new Pin {
				Type = PinType.Place,
				Position = position,
				Label = AddressLabel.Text,
				Address = DetailsLabel.Text
			};
			map.Pins.Add (pin);
		}
	}
}