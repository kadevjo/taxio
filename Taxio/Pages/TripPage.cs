﻿using Xamarin.Forms;
using Taxio.Helpers;
using System.Threading.Tasks;
using System;
using Taxio.Dependencies;
using Taxio.Services;
using Taxio.Models;
using System.Linq;
using System.Collections.Generic;
using System.Threading;

namespace Taxio.Pages
{
	public partial class TripPage : ContentPage
	{
		public int LocationErrorsCounter;
		public int ServerConnetionErrorsCounter;
		public Trip CurrentTrip;
		public bool ExitActive = true;

		public TripPage (Trip trip)
		{
			InitializeComponents ();
			Settings.Active = "True";
			Suscriptions ();
			LoadData (trip).ConfigureAwait (false);
		}

		private void Suscriptions ()
		{
			MessagingCenter.Subscribe <SendCurrentLocation,string> (this, Generals.SendCurrentLocationResult, (sender, arg) => {
				switch (arg) {
				case "LocationWarning":
					ShowAlertMessage (AppResources.LocationWarningTitle, AppResources.LocationWarningMessage);
					break;
				case "ServerConnectionWarning":
					ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
					break;
				case "Send":
					HideAlertMessage ();
					break;
				}
			});
		}

		private async Task LoadData (Trip trip)
		{
			if (trip == null) {
				var cache = DependencyService.Get<ICache> ();
				var cachedTrip = await cache.GetObjects<Trip> ();
				if (cachedTrip.Count == 0) {
					ExitActive = false;
					App.Current.MainPage = Generals.CreateNavPage (new StandByPage ());
				} else {
					CurrentTrip = cachedTrip.FirstOrDefault ();
					SetTripData ();
				}
			} else {
				var cache = DependencyService.Get<ICache> ();
				cache.InsertObjects (new List<Trip> (){ trip });
				CurrentTrip = trip;
				SetTripData ();
			}
		}

		private void SetTripData ()
		{
			AddressLabel.Text = CurrentTrip.pickup == null ? string.Empty : CurrentTrip.pickup;
			DetailsLabel.Text = CurrentTrip.pickup_at == null ? string.Empty
				: CurrentTrip.pickup_at.ToString ("HH:mm dd/MM")
			+ " - " + (string.IsNullOrEmpty (CurrentTrip.notes) ? AppResources.NoDetailsTrip : CurrentTrip.notes);
		}

		async void Code20Button_Clicked (object sender, System.EventArgs e)
		{
			//Driver has arrived at the address, and the client should be called to come out
			activityIndicator.IsVisible = true;
			await Generals.AnimateButton ((Button)sender);
			var response = await TripService.Instance.DriverArrived (CurrentTrip.id);
			if (response != default(Response) && response.success) {
				ChangeButtonStatus (sender);
				ShowSuccessMessage (AppResources.CodeSendTitle, AppResources.CodeSendMessage);
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
		}

		async void Code21Button_Clicked (object sender, System.EventArgs e)
		{
			//Call the client again
			activityIndicator.IsVisible = true;
			await Generals.AnimateButton ((Button)sender);
			var response = await TripService.Instance.CallOnce (CurrentTrip.id);
			if (response != default(Response) && response.success) {
				ChangeButtonStatus (sender);
				ShowSuccessMessage (AppResources.CodeSendTitle, AppResources.CodeSendMessage);
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
		}

		async void Code1011Button_Clicked (object sender, System.EventArgs e)
		{
			//Client is in the cab
			activityIndicator.IsVisible = true;
			await Generals.AnimateButton ((Button)sender);
			var response = await TripService.Instance.ClientPicked (CurrentTrip.id);
			if (response != default(Response) && response.success) {
				ChangeButtonStatus (sender);
				ShowSuccessMessage (AppResources.CodeSendTitle, AppResources.CodeSendMessage);
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
		}

		async void Code99Button_Clicked (object sender, System.EventArgs e)
		{
			//Client dropped at their destination, and free for a new trip
			activityIndicator.IsVisible = true;
			await Generals.AnimateButton ((Button)sender);
			var response = await TripService.Instance.ClientDropped (CurrentTrip.id);
			if (response != default(Response) && response.success) {
				// Deleting cache
				try {
					var cache = DependencyService.Get<ICache> ();
					await cache.RemoveObjects<Trip> ();
				} catch (Exception ex) {
					System.Diagnostics.Debug.WriteLine (ex.Message);
				}
				ExitActive = false;
				await DisplayAlert (AppResources.TripFinishedTitle, AppResources.TripFinishedMessage, AppResources.OK);
				App.Current.MainPage = Generals.CreateNavPage (new StandByPage ());
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
		}

		async void ContactPassengerButton_Clicked (object sender, System.EventArgs e)
		{
			//Client dropped at their destination, and free for a new trip
			activityIndicator.IsVisible = true;
			await Generals.AnimateButton ((Button)sender);
			var response = await TripService.Instance.ContactPassenger (CurrentTrip.id);
			if (response != default(Response) && response.success) {
				activityIndicator.IsVisible = false;
				ChangeButtonStatus (sender);
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
		}

		async void LogoutButton_Clicked (object sender, EventArgs e)
		{
			if (await Generals.Logout ()) {
				App.Current.MainPage = new LoginPage ();
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
		}

		private void ShowAlertMessage (string title, string message)
		{
			activityIndicator.IsVisible = false;
			TitleAlertLabel.Text = title;
			MessageAlertLabel.Text = message;
			AlertLayout.IsVisible = true;
		}

		private void HideAlertMessage ()
		{
			AlertLayout.IsVisible = false;
		}

		private async void ShowSuccessMessage (string title, string message)
		{
			activityIndicator.IsVisible = false;
			TitleSuccessLabel.Text = title;
			MessageSuccessLabel.Text = message;
			SuccessLayout.IsVisible = true;
			await SuccessLayout.FadeTo (1, 5000, Easing.CubicInOut);
			SuccessLayout.IsVisible = false;

		}

		void AddressTitleLabelTap_Tapped (object sender, System.EventArgs e)
		{
			if (CurrentTrip.geolocation == null) {
				DisplayAlert (AppResources.AlertMessageTitle, AppResources.NoGeolocationInfoWarningMessage, AppResources.OK);
			} else {
				Navigation.PushModalAsync (new MapsPage (CurrentTrip));
			}
		}

		private void ChangeButtonStatus (object sender)
		{
			var button = (Button)sender;
			button.BackgroundColor = Generals.Palette.TaxioPrimary;
			button.TextColor = Color.White;
		}

		protected override void OnDisappearing ()
		{
			if (ExitActive)
				Settings.Active = "True";
			else
				Settings.Active = "False";
		}
	}
}