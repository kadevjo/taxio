﻿using Xamarin.Forms;
using System;
using Taxio.Services;
using Taxio.Helpers;
using Taxio.Models;
using System.Net.NetworkInformation;
using Taxio.Dependencies;

namespace Taxio.Pages
{
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			App.SendCurrentLocationTask.Cancel ();
			InitializeComponents ();
		}

		async void LoginButton_Clicked (object sender, EventArgs e)
		{
			DisableControls ();
			await Generals.AnimateButton ((Button)sender);
			var generals = new Generals ();
			var user = new User {
				Company = CompanyCodeEntry.Text,
				Cab = CabCodeEntry.Text,
			};

			if (await Generals.Login (user)) {
				App.SendCurrentLocationTask.Initializer ();
				App.Current.MainPage = Generals.CreateNavPage (new StandByPage ());
			} else {
				DisplayAlert (AppResources.AlertMessageTitle, AppResources.FailLoginMessage, AppResources.OK);
				EnableControls ();
			}
		}

		private void EnableControls ()
		{
			CompanyCodeEntry.IsEnabled = true;
			CabCodeEntry.IsEnabled = true;
			LoginButton.IsEnabled = true;
		}

		private void DisableControls ()
		{
			CompanyCodeEntry.IsEnabled = false;
			CabCodeEntry.IsEnabled = false;
			LoginButton.IsEnabled = false;
		}
	}
}