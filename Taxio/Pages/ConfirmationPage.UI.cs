﻿using System;

using Xamarin.Forms;
using Taxio.Helpers;
using Xamarin.Forms.Maps;

namespace Taxio.Pages
{
	public partial class ConfirmationPage
	{
		private Button AcceptButton;
		private Button DeclineButton;
		private Button LogoutButton;
		private Label DetailsLabel;
		private Label AddressLabel;
		private Map map;
		private StackLayout AlertLayout;
		private Label TitleAlertLabel;
		private Label MessageAlertLabel;

		public void InitializeComponents ()
		{
			var MainLayout = new RelativeLayout {
				BackgroundColor = Color.White,
			};

			var ConfirmationMessageLabel = new Label {
				Text = AppResources.ConfirmationMessageText,
				FontAttributes = FontAttributes.Bold,
				TextColor = Generals.Palette.TaxioTitle,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Center,
			};
			MainLayout.Children.Add (ConfirmationMessageLabel,
				Constraint.Constant (20),
				Constraint.Constant (20),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (20)
			);

			/*var mapSpan = default(MapSpan);
			if (CurrentTrip.geolocation != null) {
				mapSpan = MapSpan.FromCenterAndRadius (new Position (CurrentTrip.geolocation.latitude, CurrentTrip.geolocation.longitude), Distance.FromMiles (0.3));
			}*/
			map = new Map () {
				IsShowingUser = true,
				HeightRequest = 100,
				WidthRequest = 960,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			MainLayout.Children.Add (map,
				Constraint.Constant (0),
				Constraint.RelativeToView (ConfirmationMessageLabel, (p, v) => v.Y + v.Height + 5),
				Constraint.RelativeToParent ((p) => p.Width),
				Constraint.Constant (220)
			);

			var AddressTitleLabel = new Label {
				Text = AppResources.Address,
				FontAttributes = FontAttributes.Bold,
				TextColor = Generals.Palette.TaxioWarning,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (AddressTitleLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (map, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);
			AddressLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Generals.Palette.TaxioTitle,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (AddressLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (AddressTitleLabel, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			var DetailsTitleLabel = new Label {
				Text = AppResources.Details,
				FontAttributes = FontAttributes.Bold,
				TextColor = Generals.Palette.TaxioWarning,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (DetailsTitleLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (AddressLabel, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			DetailsLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Generals.Palette.TaxioTitle,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (DetailsLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (DetailsTitleLabel, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			TitleAlertLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = Color.White,
			};
			MessageAlertLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Color.White,
			};
			AlertLayout = new StackLayout {
				BackgroundColor = Generals.Palette.TaxioAlert,
				Padding = new Thickness (10, 10, 10, 10),
				Children = { TitleAlertLabel, MessageAlertLabel },
				IsVisible = false,
			};
			MainLayout.Children.Add (AlertLayout,
				Constraint.Constant (20),
				Constraint.RelativeToView (DetailsLabel, (p, v) => v.Y + v.Height + 30),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (70)
			);

			AcceptButton = new Button {
				Text = AppResources.Accept,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Generals.Palette.TaxioPrimary,
				BorderRadius = 0,
			};
			AcceptButton.Clicked += AcceptButton_Clicked;
			MainLayout.Children.Add (AcceptButton,
				Constraint.Constant (20),
				Constraint.RelativeToView (AlertLayout, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 30 - (p.Width / 2)),
				Constraint.Constant (40)
			);

			DeclineButton = new Button {
				Text = AppResources.Decline,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Generals.Palette.TaxioAlert,
				BorderRadius = 0,
			};
			DeclineButton.Clicked += DeclineButton_Clicked;
			MainLayout.Children.Add (DeclineButton,
				Constraint.RelativeToView (AcceptButton, (p, v) => v.X + v.Width + 20),
				Constraint.RelativeToView (AlertLayout, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToView (AcceptButton, (p, v) => v.Width),
				Constraint.Constant (40)
			);

			Content = new ScrollView { Content = MainLayout, Padding = new Thickness (0, 0, 0, 10) };
		}
	}
}


