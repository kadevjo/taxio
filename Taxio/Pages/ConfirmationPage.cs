﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using Taxio.Dependencies;
using System.Linq;
using Taxio.Helpers;
using System;
using Taxio.Services;
using Taxio.Models;
using System.Collections.Generic;
using Refractored.Xam.Vibrate.Abstractions;

namespace Taxio.Pages
{
	public partial class ConfirmationPage : ContentPage
	{
		public Trip CurrentTrip;

		public ConfirmationPage (Trip trip)
		{
			var vibrate = DependencyService.Get<IVibrate> ();
			vibrate.Vibration ();

			// Sound disabled for FastRide
			/*var playSound = DependencyService.Get<IAudio> ();
			playSound.PlayMp3File ("new_trip.mp3");*/

			CurrentTrip = trip;
			InitializeComponents ();
			Suscriptions ();
			Settings.Active = "Waiting";
			LoadData ().ConfigureAwait (false);
		}

		private void Suscriptions ()
		{
			MessagingCenter.Subscribe <SendCurrentLocation,string> (this, Generals.SendCurrentLocationResult, (sender, arg) => {
				switch (arg) {
				case "LocationWarning":
					ShowAlertMessage (AppResources.LocationWarningTitle, AppResources.LocationWarningMessage);
					break;
				case "ServerConnectionWarning":
					ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
					break;
				case "Send":
					HideAlertMessage ();
					break;
				}
			});
		}

		public async Task LoadData ()
		{
			AddressLabel.Text = CurrentTrip.pickup == null ? string.Empty : CurrentTrip.pickup;
			DetailsLabel.Text = CurrentTrip.pickup_at == null ? string.Empty
				: CurrentTrip.pickup_at.ToString ("HH:mm dd/MM")
			+ " - " + (string.IsNullOrEmpty (CurrentTrip.notes) ? AppResources.NoDetailsTrip : CurrentTrip.notes);

			if (CurrentTrip.geolocation != null) {
				map.MoveToRegion (
					MapSpan.FromCenterAndRadius (
						new Position (CurrentTrip.geolocation.latitude, CurrentTrip.geolocation.longitude),
						Distance.FromMiles (0.3)
					)
				);
			} else {
				var generals = new Generals ();
				var currentPosition = await generals.CurrentLocation ();
				map.MoveToRegion (
					MapSpan.FromCenterAndRadius (
						new Position (currentPosition [0], currentPosition [1]),
						Distance.FromMiles (0.3)
					)
				);
			}

			var position = new Position (CurrentTrip.geolocation.latitude, CurrentTrip.geolocation.longitude);
			var pin = new Pin {
				Type = PinType.Place,
				Position = position,
				Label = AddressLabel.Text,
				Address = DetailsLabel.Text
			};
			map.Pins.Add (pin);
		}

		async void DeclineButton_Clicked (object sender, EventArgs e)
		{
			DisableControls ();
			await Generals.AnimateButton ((Button)sender);

			var response = await TripService.Instance.DeclineTrip (CurrentTrip.id);
			if (response != default(Response) && response.success) {
				App.Current.MainPage = Generals.CreateNavPage (new StandByPage ());
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
			EnableControls ();
		}

		async void AcceptButton_Clicked (object sender, EventArgs e)
		{
			DisableControls ();
			await Generals.AnimateButton ((Button)sender);

			var response = await TripService.Instance.AcceptTrip (CurrentTrip.id);
			if (response != default(Response) && response.success) {
				if (!response.trip_assigned) {
					var trip = response.trip;
					App.Current.MainPage = Generals.CreateNavPage (new TripPage (CurrentTrip));				
				} else {
					DisplayAlert (AppResources.TripTakenWarningTitle, AppResources.TripTakenWarningMessage, AppResources.OK);
					App.Current.MainPage = Generals.CreateNavPage (new StandByPage ());
				}				
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
			EnableControls ();
		}

		private void DisableControls ()
		{
			AcceptButton.IsEnabled = false;
			DeclineButton.IsEnabled = false;
		}

		private void EnableControls ()
		{
			AcceptButton.IsEnabled = true;
			DeclineButton.IsEnabled = true;
		}

		private void ShowAlertMessage (string title, string message)
		{
			TitleAlertLabel.Text = title;
			MessageAlertLabel.Text = message;
			AlertLayout.IsVisible = true;
		}

		private void HideAlertMessage ()
		{
			AlertLayout.IsVisible = false;
		}

		protected override void OnDisappearing ()
		{
			Settings.Active = "False";
		}
	}
}