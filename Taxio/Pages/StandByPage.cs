﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using Taxio.Helpers;
using Taxio.Services;
using Taxio.Models;
using Apps = Xamarin.Forms.Application;
using Taxio.Dependencies;
using System.Collections.Generic;

namespace Taxio.Pages
{
	public partial class StandByPage : ContentPage
	{
		public StandByPage ()
		{
			if (App.Args != null && App.Args.trip != null)
				App.SearchTripTask.NewTripHandler (App.Args);

			InitializeComponents ();
			Suscriptions ();
			App.SearchTripTask.Initializer ();
			Settings.Active = "False";
		}

		private void Suscriptions ()
		{
			MessagingCenter.Subscribe <SendCurrentLocation,string> (this, Generals.SendCurrentLocationResult, (sender, arg) => {
				switch (arg) {
				case "LocationWarning":
					ShowAlertMessage (AppResources.LocationWarningTitle, AppResources.LocationWarningMessage);
					break;
				case "ServerConnectionWarning":
					ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
					break;
				case "Send":
					HideAlertMessage ();
					break;
				}
			});

			MessagingCenter.Subscribe <SearchTrip,Dictionary<string, object>> (this, Generals.SearchTripResult, (sender, arg) => {
				var message = (string)arg ["message"];

				switch (message) {
				case "ServerConnectionWarning":
					ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
					break;
				}
			});
		}

		async void LogoutButton_Clicked (object sender, EventArgs e)
		{
			if (await Generals.Logout ()) {
				App.Current.MainPage = new LoginPage ();
			} else {
				ShowAlertMessage (AppResources.ServerConnectionWarningTitle, AppResources.ServerConnectionWarningMessage);
			}
		}

		private void ShowAlertMessage (string title, string message)
		{
			TitleAlertLabel.Text = title;
			MessageAlertLabel.Text = message;
			AlertLayout.IsVisible = true;
		}

		private void HideAlertMessage ()
		{
			AlertLayout.IsVisible = false;
		}

		protected override void OnDisappearing ()
		{
			App.SearchTripTask.Cancel ();
		}
	}
}