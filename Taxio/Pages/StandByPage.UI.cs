﻿using Xamarin.Forms;
using Taxio.Helpers;

namespace Taxio.Pages
{
	public partial class StandByPage
	{
		private Button SimulateTripRequestButton;
		private StackLayout AlertLayout;
		private Label TitleAlertLabel;
		private Label MessageAlertLabel;

		public void InitializeComponents ()
		{
			Title = "Fast Ride";

			var MainLayout = new RelativeLayout () {
				BackgroundColor = Generals.Palette.TaxioStandByBackground,
			};

			/*var background = new Image {
				Source = "background_standby.png",
				Aspect = Aspect.AspectFill,
			};
			MainLayout.Children.Add (background,
				Constraint.Constant (0),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((p) => p.Width),
				Constraint.RelativeToParent ((p) => p.Height)
			);*/

			var StandingByImage = new Image {
				Source = "item_stand_by.png",
				Aspect = Aspect.AspectFit,
			};
			MainLayout.Children.Add (StandingByImage,
				Constraint.Constant (20),
				Constraint.RelativeToParent ((p) => p.Height / 2 - 100 - 70),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (200)
			);

			var standingByLabel = new Label {
				Text = AppResources.StandingBy,
				XAlign = TextAlignment.Center,
				TextColor = Generals.Palette.TaxioPrimary,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
			};
			MainLayout.Children.Add (standingByLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (StandingByImage, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			var LogoutToolbarButton = new ToolbarItem {
				Icon = "ic_logout.png",
			};
			LogoutToolbarButton.Clicked += LogoutButton_Clicked;
			ToolbarItems.Add (LogoutToolbarButton);

			TitleAlertLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = Color.White,
			};
			MessageAlertLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Color.White,
			};
			AlertLayout = new StackLayout {
				BackgroundColor = Generals.Palette.TaxioAlert,
				Padding = new Thickness (10, 10, 10, 10),
				Children = { TitleAlertLabel, MessageAlertLabel },
				IsVisible = false,
			};
			MainLayout.Children.Add (AlertLayout,
				Constraint.Constant (0),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((p) => p.Width),
				Constraint.Constant (70)
			);

			Content = MainLayout;
		}
	}
}