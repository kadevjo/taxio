﻿using System;

using Xamarin.Forms;
using Taxio.Helpers;
using Xamarin.Forms.Maps;

namespace Taxio.Pages
{
	public partial class MapsPage
	{
		private Label DetailsLabel;
		private Label AddressLabel;
		private Map map;

		public void InitializeComponents ()
		{
			var MainLayout = new RelativeLayout () {
				BackgroundColor = Color.FromHex ("#FFFFFF"),
			};

			map = new Map (MapSpan.FromCenterAndRadius (new Position (CurrentTrip.geolocation.latitude, CurrentTrip.geolocation.longitude), Distance.FromMiles (0.3))) {
				IsShowingUser = true,
				HeightRequest = 100,
				WidthRequest = 960,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			MainLayout.Children.Add (map,
				Constraint.Constant (0),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((p) => p.Width),
				Constraint.RelativeToParent ((p) => p.Height - 140)
			);

			var AddressTitleLabel = new Label {
				Text = AppResources.Address,
				FontAttributes = FontAttributes.Bold,
				TextColor = Generals.Palette.TaxioWarning,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (AddressTitleLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (map, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);
			AddressLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Generals.Palette.TaxioTitle,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (AddressLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (AddressTitleLabel, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			var DetailsTitleLabel = new Label {
				Text = AppResources.Details,
				FontAttributes = FontAttributes.Bold,
				TextColor = Generals.Palette.TaxioWarning,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (DetailsTitleLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (AddressLabel, (p, v) => v.Y + v.Height + 10),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			DetailsLabel = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Generals.Palette.TaxioTitle,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (DetailsLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (DetailsTitleLabel, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			Content = new ScrollView { Content = MainLayout };
		}
	}
}