﻿using System;

using Xamarin.Forms;
using Taxio.Helpers;
using Taxio.Controls;

namespace Taxio.Pages
{
	public partial class LoginPage
	{
		private Entry CompanyCodeEntry;
		private Entry CabCodeEntry;
		private Button LoginButton;

		public void InitializeComponents ()
		{
			var MainLayout = new RelativeLayout {
				BackgroundColor = Color.White,
			};

			var background = new Image {
				Source = "background_login.png",
				Aspect = Aspect.AspectFill,
			};
			MainLayout.Children.Add (background,
				Constraint.Constant (0),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((p) => p.Width),
				Constraint.RelativeToParent ((p) => p.Height)
			);

			var LogoImage = new Image {
				Source = "item_logo.png",
				Aspect = Aspect.AspectFit,
			};
			MainLayout.Children.Add (LogoImage,
				Constraint.Constant (20),
				Constraint.Constant (50),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (150)
			);

			var WelcomeMessageLabel = new Label {
				Text = AppResources.WelcomeMessageText,
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Color.White,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Center,
			};
			MainLayout.Children.Add (WelcomeMessageLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (LogoImage, (p, v) => v.Y + v.Height + 20),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			var CompanyCodeLabel = new Label {
				Text = AppResources.CompanyCode,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (CompanyCodeLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (WelcomeMessageLabel, (p, v) => v.Y + v.Height + 40),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			CompanyCodeEntry = new FlatEntry {
				BackgroundColor = Color.White,
				Opacity = 0.8,
				TextColor = Color.Gray,
			};
			MainLayout.Children.Add (CompanyCodeEntry,
				Constraint.Constant (20),
				Constraint.RelativeToView (CompanyCodeLabel, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (40)
			);

			var DrivePhoneNumberLabel = new Label {
				Text = AppResources.CabCode,
				FontAttributes = FontAttributes.Bold,
				TextColor = Color.White,
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Start,
			};
			MainLayout.Children.Add (DrivePhoneNumberLabel,
				Constraint.Constant (20),
				Constraint.RelativeToView (CompanyCodeEntry, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (30)
			);

			CabCodeEntry = new FlatEntry {
				BackgroundColor = Color.White,
				Opacity = 0.8,
				TextColor = Color.Gray,

			};
			//DrivePhoneNumberEntry.Layout (new Rectangle (5, 5, 15, 15));

			MainLayout.Children.Add (CabCodeEntry,
				Constraint.Constant (20),
				Constraint.RelativeToView (DrivePhoneNumberLabel, (p, v) => v.Y + v.Height),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (40)
			);

			LoginButton = new Button {
				Text = AppResources.Login,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Generals.Palette.TaxioPrimary,
				BorderRadius = 0,
			};
			LoginButton.Clicked += LoginButton_Clicked;
			MainLayout.Children.Add (LoginButton,
				Constraint.Constant (20),
				Constraint.RelativeToView (CabCodeEntry, (p, v) => v.Y + v.Height + 20),
				Constraint.RelativeToParent ((p) => p.Width - 40),
				Constraint.Constant (40)
			);

			Content = new ScrollView{ Content = MainLayout };
		}
	}
}

