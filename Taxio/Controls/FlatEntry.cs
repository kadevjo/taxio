﻿using System;
using Xamarin.Forms;

namespace Taxio.Controls
{
	public class FlatEntry : Entry
	{
		public static readonly BindableProperty PlaceholderColorProperty = 
			BindableProperty.Create<FlatEntry,Color> (
				p => p.PlaceholderColor, Color.Gray
			);

		public Color PlaceholderColor {
			get { return (Color)GetValue (PlaceholderColorProperty); }
			set { SetValue (PlaceholderColorProperty, value); }
		}
	}
}


