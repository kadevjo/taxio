﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Parse;
using Xamarin.Forms;

using Apps = Xamarin.Forms.Application;
using Newtonsoft.Json;
using Taxio.Models;
using Taxio.Helpers;
using Taxio.Pages;

namespace Taxio.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			UIApplication.SharedApplication.IdleTimerDisabled = true;

			// Code for starting up the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start ();
			#endif

			Xamarin.FormsMaps.Init ();
			LoadApplication (new App ());

			ParseClient.Initialize (Settings.ParseAppId, Settings.ParseAppKey);
			RegisterNotifications ();

			return base.FinishedLaunching (app, options);
		}

		public override void DidReceiveRemoteNotification (UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			application.ApplicationIconBadgeNumber = 0;
			Response trip = ProcessNotification (userInfo);

			if (application.ApplicationState == UIApplicationState.Active) {                
				if (!string.IsNullOrEmpty (trip.trip.id) && Settings.Active == "False") {
					App.Current.MainPage = new ConfirmationPage (trip.trip);
				}
			} else {
				App.Args = trip;
			}
		}

		public void RegisterNotifications ()
		{
			UIApplication app = UIApplication.SharedApplication;

			var version = new Version (UIDevice.CurrentDevice.SystemVersion);
			if (version >= new Version ("8.0")) {
				UIUserNotificationType notificationTypes = 
					UIUserNotificationType.Alert |
					UIUserNotificationType.Badge |
					UIUserNotificationType.Sound;
				UIUserNotificationSettings settings = UIUserNotificationSettings.GetSettingsForTypes (notificationTypes, null);
				app.RegisterUserNotificationSettings (settings);
				app.RegisterForRemoteNotifications ();
			} else {
				UIRemoteNotificationType notificationTypes = 
					UIRemoteNotificationType.Alert |
					UIRemoteNotificationType.Badge |
					UIRemoteNotificationType.Sound;
				app.RegisterForRemoteNotificationTypes (notificationTypes);
			}
		}

		public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
		{
			// Store the deviceToken in the current installation and save it to Parse.
			ParseInstallation currentInstallation = ParseInstallation.CurrentInstallation;
			currentInstallation.SetDeviceTokenFromData (deviceToken);
			currentInstallation.Channels = new List<string> (){ "global" };
			currentInstallation.SaveAsync ();
		}

		private Response ProcessNotification (NSDictionary options)
		{
			// Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
			if (options != null && options.ContainsKey (new NSString ("aps"))) {
				Response response = new Response () { 
					success = (options.ValueForKeyPath (new NSString ("success")) as NSNumber).BoolValue,
					trip_assigned = (options.ValueForKeyPath (new NSString ("trip_assigned")) as NSNumber).BoolValue
				};

				if (options.ContainsKey (new NSString ("trip"))) {
					response.trip = new Trip () {
						id = (options.ValueForKeyPath (new NSString ("trip.id")) as NSString).ToString (),
						notes = (options.ValueForKeyPath (new NSString ("trip.notes")) as NSString).ToString (),
						pickup = (options.ValueForKeyPath (new NSString ("trip.pickup")) as NSString).ToString (),
						pickup_at = DateTime.Parse ((options.ValueForKeyPath (new NSString ("trip.pickup_at")) as NSString).ToString ()),
						geolocation = new Location () {
							latitude = (options.ValueForKeyPath (new NSString ("trip.geolocation.latitude")) as NSNumber).DoubleValue,
							longitude = (options.ValueForKeyPath (new NSString ("trip.geolocation.longitude")) as NSNumber).DoubleValue
						}
					};
				} else if (options.ContainsKey (new NSString ("error"))) { 
					response.error = new Error () {
						message = (options.ValueForKeyPath (new NSString ("error.message")) as NSString).ToString (),
						code = (options.ValueForKeyPath (new NSString ("error.code")) as NSString).ToString ()
					};
				}

				return response;
			}

			return null;
		}
	}
}

