﻿using Xamarin.Forms;
using Taxio.Dependencies;
using AudioToolbox;
using System;
using Taxio.iOS.Dependencies;


[assembly:Dependency (typeof(AudioService_iOS))]
namespace Taxio.iOS.Dependencies
{
	public class AudioService_iOS: IAudio
	{
		public AudioService_iOS ()
		{
		}

		public bool PlayMp3File (string fileName)
		{
			try {
				AudioSession.Initialize ();   
				AudioSession.Category = AudioToolbox.AudioSessionCategory.MediaPlayback;   
				AudioSession.SetActive (true);

				var t = SystemSound.FromFile ("new_trip.mp3");
				t.PlaySystemSound ();
			} catch (Exception ex) {
				Console.WriteLine (ex.Message);
			}

			/*systemSound = new SystemSound (NSUrl.FromFilename ("new_trip.mp3"));
			systemSound.PlayAlertSound ();*/
			return true;
		}
	}
}