using Xamarin.Forms;
using Taxio.Dependencies;
using Taxio.iOS.Dependencies;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xamarin.Geolocation;
using System;
using System.Net.NetworkInformation;
using Foundation;
using UIKit;
using Parse;

[assembly:Dependency (typeof(GeneralServices))]

namespace Taxio.iOS.Dependencies
{
	public class GeneralServices : IGeneralServices
	{
		public async Task<Dictionary<string, double>> GetUserLocation ()
		{
			var locator = new Geolocator () { DesiredAccuracy = 1000 };
			try {
				var position = await locator.GetPositionAsync (timeout: 10000);
				var dictionary = new Dictionary<string, double> ();
				dictionary.Add ("Latitude", position.Latitude);
				dictionary.Add ("Longitude", position.Longitude);
				return dictionary;
			} catch (Exception ex) {
				Console.WriteLine (ex.Message);
				return new Dictionary<string, double> ();
			}
		}

		public void ClearNotifications()
		{
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
			UIApplication.SharedApplication.CancelAllLocalNotifications ();
		}

		public string GetDeviceId ()
		{
			var id = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();
			return id;
		}

		public async Task IdentifyInstalltion (string push_id)
		{
			ParseInstallation.CurrentInstallation ["DRIVER_CODIGO"] = push_id;
			ParseInstallation.CurrentInstallation.SaveAsync ();

		}
	}
}