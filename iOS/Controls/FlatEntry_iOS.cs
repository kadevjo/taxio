﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Foundation;
using System.ComponentModel;
using Taxio.Controls;
using System.Drawing;
using System;

[assembly: ExportRenderer (typeof(FlatEntry), typeof(Taxio.iOS.Controls.FlatEntry_iOS))]
namespace Taxio.iOS.Controls
{
	public class FlatEntry_iOS : EntryRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement != null || Element == null)
				return;
			var entry = (FlatEntry)e.NewElement;

			var placeholder = string.IsNullOrEmpty (entry.Placeholder) ? string.Empty : entry.Placeholder;

			Control.BackgroundColor = entry.BackgroundColor.ToUIColor ();

			Control.AttributedPlaceholder = new NSAttributedString (
				placeholder,
				foregroundColor: entry.PlaceholderColor.ToUIColor ()
			);

			Control.BorderStyle = UITextBorderStyle.None;
		}

		protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
			if (this == null)
				return;
			
			var entry = (FlatEntry)this.Element;

			Control.BorderStyle = UITextBorderStyle.None;

			if (e.PropertyName == FlatEntry.PlaceholderProperty.PropertyName) {
				var placeholder = string.IsNullOrEmpty (entry.Placeholder) ? string.Empty : entry.Placeholder;
				Control.AttributedPlaceholder = new NSAttributedString (
					placeholder,
					foregroundColor: UIColor.DarkGray
				);
			} else if (e.PropertyName == FlatEntry.BackgroundColorProperty.PropertyName) {
				Control.BackgroundColor = entry.BackgroundColor.ToUIColor ();
			}
		}
	}
}