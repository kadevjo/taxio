﻿using Xamarin.Forms;
using Taxio.Controls;
using Xamarin.Forms.Platform.Android;
using System.ComponentModel;
using Taxio.Controls;

[assembly: ExportRenderer (typeof(Taxio.Controls.FlatEntry), typeof(Taxio.Droid.Controls.DroidFlatEntry))]
namespace Taxio.Droid.Controls
{
	public class DroidFlatEntry : EntryRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement != null || Element == null)
				return;
			var entry = (FlatEntry)e.NewElement;

			var placeholder = string.IsNullOrEmpty (entry.Placeholder) ? string.Empty : entry.Placeholder;

			Control.SetBackgroundColor (entry.BackgroundColor.ToAndroid ());

			//Control.SetTextColor = entry.PlaceholderColor.ToAndroid ();
		}

		protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
			if (this == null)
				return;
			var entry = (FlatEntry)this.Element;


//			if (e.PropertyName == FlatEntry.PlaceholderProperty.PropertyName) {
//				var placeholder = string.IsNullOrEmpty (entry.Placeholder) ? string.Empty : entry.Placeholder;
//				Control.AttributedPlaceholder = new NSAttributedString (
//					placeholder,
//					foregroundColor: UIColor.DarkGray
//				);
//			} else
			if (e.PropertyName == FlatEntry.BackgroundColorProperty.PropertyName) {
				Control.SetBackgroundColor (entry.BackgroundColor.ToAndroid ());
			}
		}
	}
}

