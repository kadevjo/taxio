﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics.Drawables;

namespace Taxio.Droid
{
	[Activity (Label = "Fast Ride", Icon = "@android:color/transparent", Theme = "@style/TaxioActionBarTheme",
		ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		public static MainActivity Instance;
		public static Context AppContext;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			this.Window.SetFlags (WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);

			global::Xamarin.Forms.Forms.Init (this, bundle);
			Xamarin.FormsMaps.Init (this, bundle);

			Instance = this;
			AppContext = this;

			LoadApplication (new App ());
		}
	}
}