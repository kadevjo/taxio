using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Taxio.Dependencies;
using Taxio.Droid.Dependencies;
using Xamarin.Forms;
using Xamarin.Geolocation;
using Android.Runtime;
using Android.App;
using Parse;

[assembly:Dependency (typeof(GeneralServices))]
namespace Taxio.Droid.Dependencies
{
	public class GeneralServices : IGeneralServices
	{
		public async Task<Dictionary<string, double>> GetUserLocation ()
		{
			var locator = new Geolocator (MainActivity.AppContext) { DesiredAccuracy = 1000 };
			try {
				var position = await locator.GetPositionAsync (timeout: 10000);
				var dictionary = new Dictionary<string, double> ();
				dictionary.Add ("Latitude", position.Latitude);
				dictionary.Add ("Longitude", position.Longitude);
				return dictionary;
			} catch (Exception ex) {
				Console.WriteLine (ex.Message);
				return new Dictionary<string, double> ();
			}
		}

		public void ClearNotifications ()
		{
			NotificationManager manager = NotificationManager.FromContext (Forms.Context);
			manager.CancelAll ();
		}

		public string GetDeviceId ()
		{
			var id = Android.OS.Build.Serial;
			return id;
		}

		public async Task IdentifyInstalltion (string push_id)
		{
			ParseInstallation.CurrentInstallation ["DRIVER_CODIGO"] = push_id;
			ParseInstallation.CurrentInstallation.SaveAsync ();

		}
	}
}