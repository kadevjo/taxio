﻿using Xamarin.Forms;
using Refractored.Xam.Forms.Vibrate.Droid;
using Refractored.Xam.Vibrate.Abstractions;
using Android.OS;
using System;
using Android.Content;

[assembly:Dependency (typeof(Vibrate_Android))]
namespace Refractored.Xam.Forms.Vibrate.Droid
{
	/// <summary>
	/// Vibration Implentation on Android
	/// </summary>
	public class Vibrate_Android : IVibrate
	{
		/// <summary>
		/// Initialization code for Vibrate
		/// </summary>
		public static void Init ()
		{
		}

		/// <summary>
		/// Vibrate device for specified amount of time
		/// </summary>
		/// <param name="milliseconds">Time in MS (500ms is default).</param>
		public void Vibration (int milliseconds = 500)
		{
			using (var v = (Vibrator)Xamarin.Forms.Forms.Context.GetSystemService (Context.VibratorService)) {

				if (!v.HasVibrator) {
					Console.WriteLine ("Android device does not have vibrator.");
					return;
				}

				if (milliseconds < 0)
					milliseconds = 0;

				try {
					v.Vibrate (milliseconds);
				} catch (Exception ex) {
					Console.WriteLine ("Unable to vibrate Android device, ensure VIBRATE permission is set.");
				}
			}

		}
	}
}