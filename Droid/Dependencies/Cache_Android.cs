﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Akavache;
using System.Reactive.Linq;
using System.Linq;
using System.Collections.ObjectModel;
using Taxio.Droid.Dependencies;
using Taxio.Dependencies;

[assembly: Xamarin.Forms.Dependency(typeof(Cache_Android))]
namespace Taxio.Droid.Dependencies
{
	public class Cache_Android : ICache
	{
		public Cache_Android ()
		{
			BlobCache.ApplicationName = "FastRide";
		}

		public async Task<List<T>> GetObjects<T> () where T : ICacheable
		{
			IEnumerable<T> objects = await BlobCache.LocalMachine.GetAllObjects<T>();
			objects.ToList ();
			return new List<T>(objects);
		}

		public async Task<T> GetObject<T> (string key) where T : ICacheable
		{
			try {
				return await BlobCache.LocalMachine.GetObject<T>(key);
			} catch (KeyNotFoundException){
				return default(T);
			}
		}

		public async Task InsertObjects<T> (List<T> objects) where T : ICacheable
		{
			Dictionary<string,T> values = new Dictionary<string, T> ();
			foreach( var @object in objects ) {
				values.Add ( @object.CacheId, @object );
			}

			await BlobCache.LocalMachine.InsertObjects<T> (values);
		}

		public async Task InsertObject<T> (T @object) where T : ICacheable
		{
			await BlobCache.LocalMachine.InsertObject<T> (@object.CacheId, @object);
		}

		public async Task RemoveObjects<T> () where T : ICacheable
		{
			await BlobCache.LocalMachine.InvalidateAllObjects<T> ();
		}

		public async Task RemoveObject<T> (string key) where T : ICacheable
		{
			await BlobCache.LocalMachine.InvalidateObject<T>(key);
		}
	}
}