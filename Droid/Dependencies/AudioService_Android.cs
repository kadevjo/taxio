﻿using System;
using Xamarin.Forms;
using Taxio.Droid.Dependencies;
using Android.Media;
using Taxio.Dependencies;

[assembly:Dependency (typeof(AudioService_Android))]
namespace Taxio.Droid.Dependencies
{
	public class AudioService_Android : IAudio
	{
		public AudioService_Android ()
		{
		}

		private MediaPlayer _mediaPlayer;

		public bool PlayMp3File (string fileName)
		{
			_mediaPlayer = MediaPlayer.Create (global::Android.App.Application.Context, Resource.Raw.new_trip);
			_mediaPlayer.Start ();

			return true;
		}
	}
}