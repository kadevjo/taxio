﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;

using Xamarin.Forms;

using Parse;

using Apps = Xamarin.Forms.Application;
using Android.Media;
using Taxio.Helpers;
using Taxio.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Taxio.Pages;

namespace Taxio.Droid
{
	[Application (Name = "taxiodroid.Taxio")]
	public class Taxio : Android.App.Application, Android.App.Application.IActivityLifecycleCallbacks
	{
		private bool applicationOpened;

		public Taxio (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer)
		{
		}

		public override void OnCreate ()
		{
			base.OnCreate ();
			RegisterActivityLifecycleCallbacks (this);

			ParseClient.Initialize (Settings.ParseAppId, Settings.ParseAppKey);
			ParseInstallation.CurrentInstallation.SaveAsync ();
			ParsePush.ParsePushNotificationReceived += ProcessNotification;
		}

		private void ProcessNotification (object IntentSender, ParsePushNotificationEventArgs args)
		{
			var payload = new Dictionary<string,object> (args.Payload);

			var jsonTrip = JsonConvert.SerializeObject (payload);
			var trip = JsonConvert.DeserializeObject<Response> (jsonTrip);

			if (applicationOpened) {
				if (!string.IsNullOrEmpty (trip.trip.id) && Settings.Active == "False") {
					App.Current.MainPage = new ConfirmationPage (trip.trip);
				}
			} else {
				if (Settings.Active == "False")
					CreateNotification ("Fast Ride", "Nuevo Viaje Disponible", payload);
			}
		}

		public void CreateNotification (string title, string description, Dictionary<string,object> payload)
		{
			var notificationManager = GetSystemService (Context.NotificationService) as NotificationManager;
			Notification.Builder builder = new Notification.Builder (ApplicationContext);

			var jsonTrip = JsonConvert.SerializeObject (payload);
			var trip = JsonConvert.DeserializeObject<Response> (jsonTrip);
			App.Args = trip;

			Intent result = new Intent (ApplicationContext, typeof(SplashActivity));
			TaskStackBuilder stack = TaskStackBuilder.Create (ApplicationContext);
			stack.AddNextIntent (result);
			PendingIntent resultPendingIntent = stack.GetPendingIntent (0, PendingIntentFlags.UpdateCurrent);
			builder.SetContentIntent (resultPendingIntent);

			builder.SetContentTitle (title);
			builder.SetContentText (description);
			builder.SetSmallIcon (Resource.Drawable.icon);
			Bitmap icon = BitmapFactory.DecodeResource (Context.Resources, Resource.Drawable.icon);
			builder.SetLargeIcon (icon);
			builder.SetStyle (new Notification.BigTextStyle ().BigText (description));

			// Sound disabled for FastRide
			/*string pathToPushSound = "android.resource://" + this.ApplicationContext.PackageName + "/raw/new_trip";
			Android.Net.Uri soundUri = Android.Net.Uri.Parse (pathToPushSound);
			builder.SetSound (soundUri);*/
			builder.SetSound (RingtoneManager.GetDefaultUri (RingtoneType.Notification));

			notificationManager.Notify (1, builder.Build ());
		}

		public void OnActivityCreated (Activity activity, Bundle savedInstanceState)
		{
			Console.WriteLine (activity.LocalClassName + " Created");
		}

		public void OnActivityDestroyed (Activity activity)
		{
			Console.WriteLine (activity.LocalClassName + " Destroyed");
		}

		public void OnActivityPaused (Activity activity)
		{
			Console.WriteLine (activity.LocalClassName + " Paused");
			applicationOpened = false;
		}

		public void OnActivityResumed (Activity activity)
		{
			Console.WriteLine (activity.LocalClassName + " Resumed");
			applicationOpened = true;
		}

		public void OnActivitySaveInstanceState (Activity activity, Bundle outState)
		{
			Console.WriteLine (activity.LocalClassName + " Saved");
		}

		public void OnActivityStarted (Activity activity)
		{
			Console.WriteLine (activity.LocalClassName + " Started");
		}

		public void OnActivityStopped (Activity activity)
		{
			Console.WriteLine (activity.LocalClassName + " Stopped");
		}
	}
}